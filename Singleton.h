/*
 * getInstance.h
 *
 *  Created on: 10.05.2017
 *      Author: Magda
 */

#ifndef SINGLETON_H_
#define SINGLETON_H_

class Singleton {
public:
	static Singleton *instanceMethod (/*getInstance *_obj*/);
	virtual ~Singleton();

//protected:
//	static bool exist;

//attributes
private:
	static Singleton *obj;
	Singleton();


};

#endif /* SINGLETON_H_ */
