/*
 * getInstance.cpp
 *
 *  Created on: 10.05.2017
 *      Author: Magda
 */

#include "Singleton.h"


Singleton* Singleton::obj=0;

Singleton::Singleton() {

	// TODO Auto-generated constructor stub

}


Singleton::~Singleton() {
	// TODO Auto-generated destructor stub
}

Singleton *Singleton::instanceMethod() {

	if(obj==0) {
		obj=new Singleton;
	}
	return obj;
}

